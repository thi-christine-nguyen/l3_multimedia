// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char *argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille, nR, nG, nB;

    if (argc != 3) 
    {
       printf("Usage: ImageIn.ppm ImageOut.ppm \n"); 
       exit (1) ;
    }

    sscanf (argv[1],"%s",cNomImgLue) ;
    sscanf (argv[2],"%s",cNomImgEcrite);



    OCTET *ImgIn, *ImgOut;

    lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;

    int nTaille3 = nTaille * 3;
    allocation_tableau(ImgIn, OCTET, nTaille3);
    lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille3);

    int p = indiceColImgCouleur(15, 3);
    int t = (3*3)-3; 

    printf(" p = %d\n", p);
    printf(" t = %d\n", t);

    /*
    for(int i = 0; i<nTaille3; i++){
        ImgOut[i] = ImgIn[i]; 
    }*/

    
    for (int i=0; i < nTaille3; i+=3)
    {
        int ligneR = indiceLineImgCouleur(i, nW); 
        int colR = indiceColImgCouleur(i, nW); 

        int ligneG = indiceLineImgCouleur(i+1, nW); 
        int colG = indiceColImgCouleur(i+1, nW); 

        int ligneB = indiceLineImgCouleur(i+2, nW); 
        int colB = indiceColImgCouleur(i+2, nW); 

        if(indiceLineImgCouleur(i, nW)== 0){
            ImgOut[i] = ImgIn[i];
            ImgOut[i+1] = ImgIn[i+1];
            ImgOut[i+2] = ImgIn[i+2];
        }else if(indiceLineImgCouleur(i, nW)== nH-1){
   
            ImgOut[i] = ImgIn[i];
            ImgOut[i+1] = ImgIn[i+1];
            ImgOut[i+2] = ImgIn[i+2];
        }else if(indiceColImgCouleur(i, nW)== 0){
        
            ImgOut[i] = ImgIn[i];
            ImgOut[i+1] = ImgIn[i+1];
            ImgOut[i+2] = ImgIn[i+2];
        }else if(indiceColImgCouleur(i, nW) >= (3*nW)-3){//4  = lennW + 3RBG
            ImgOut[i] = ImgIn[i];
            ImgOut[i+1] = ImgIn[i+1];
            ImgOut[i+2] = ImgIn[i+2];
        }
        else {

            
            ImgOut[i] = (ImgIn[getIndiceIjImgCouleur(ligneR, colR-3, nW, nH)] + ImgIn[getIndiceIjImgCouleur(ligneR, colR+3, nW, nH)] + 
                        ImgIn[getIndiceIjImgCouleur(ligneR-1, colR, nW, nH)] + ImgIn[getIndiceIjImgCouleur(ligneR+1, colR, nW, nH)] +
                        ImgIn[getIndiceIjImgCouleur(ligneR-1, colR-3, nW, nH)] + ImgIn[getIndiceIjImgCouleur(ligneR-1, colR+3, nW, nH)] +
                        ImgIn[getIndiceIjImgCouleur(ligneR+1, colR-3, nW, nH)] + ImgIn[getIndiceIjImgCouleur(ligneR+1, colR+3, nW, nH)] +
                        ImgIn[getIndiceIjImgCouleur(ligneR, colR, nW, nH)])/9; 

            ImgOut[i+1] = (ImgIn[getIndiceIjImgCouleur(ligneG, colG-3, nW, nH)] + ImgIn[getIndiceIjImgCouleur(ligneG, colG+3, nW, nH)] + 
                        ImgIn[getIndiceIjImgCouleur(ligneG-1, colG, nW, nH)] + ImgIn[getIndiceIjImgCouleur(ligneG+1, colG, nW, nH)] +
                        ImgIn[getIndiceIjImgCouleur(ligneG-1, colG-3, nW, nH)] + ImgIn[getIndiceIjImgCouleur(ligneG-1, colG+3, nW, nH)] +
                        ImgIn[getIndiceIjImgCouleur(ligneG+1, colG-3, nW, nH)] + ImgIn[getIndiceIjImgCouleur(ligneG+1, colG+3, nW, nH)] +
                        ImgIn[getIndiceIjImgCouleur(ligneG, colG, nW, nH)])/9; 
            
            ImgOut[i+2] = (ImgIn[getIndiceIjImgCouleur(ligneB, colB-3, nW, nH)] + ImgIn[getIndiceIjImgCouleur(ligneB, colB+3, nW, nH)] + 
                        ImgIn[getIndiceIjImgCouleur(ligneB-1, colB, nW, nH)] + ImgIn[getIndiceIjImgCouleur(ligneB+1, colB, nW, nH)] +
                        ImgIn[getIndiceIjImgCouleur(ligneB-1, colB-3, nW, nH)] + ImgIn[getIndiceIjImgCouleur(ligneB-1, colB+3, nW, nH)] +
                        ImgIn[getIndiceIjImgCouleur(ligneB+1, colB-3, nW, nH)] + ImgIn[getIndiceIjImgCouleur(ligneB+1, colB+3, nW, nH)] +
                        ImgIn[getIndiceIjImgCouleur(ligneB, colB, nW, nH)])/9; 
        }

        //i - 3*nW >= 0 il a un voisin au dessus
        
    }

    ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
    free(ImgIn);

    return 1;
}
