
#include <stdio.h>
#include "image_ppm.h"
//plot 'histoPeppersRGB.dat' using 1:2 with lines lt rgb "red" title "canal rouge", 'histoPeppersRGB.dat' using 1:3 with lines lt rgb "green" title "canal vert",  
//'histoPeppersRGB.dat' using 1:4 with lines lt rgb "blue" title "canal bleu"

/*plot 'histoPeppersRGB.dat' using 1:2 with lines lt rgb "red" title "canal rouge", 'histoPeppersRGB.dat' using 1:3 with lines lt rgb "green" title "canal vert",  'histoPeppersRGB.dat' using 1:4 with lines lt rgb "blue" title "canal bleu"*/
/*plot 'histoBaboonRGB.dat' using 1:2 with lines lt rgb "red" title "canal rouge", 'histoBaboonRGB.dat' using 1:3 with lines lt rgb "green" title "canal vert",  'histoBaboonRGB.dat' using 1:4 with lines lt rgb "blue" title "canal bleu"*/


/*plot 'histoCouleur.dat' using 1:2 with lines lt rgb "red" title "canal rouge", 'histoCouleur.dat' using 1:3 with lines lt rgb "green" title "canal vert",  'histoCouleur.dat' using 1:4 with lines lt rgb "blue" title "canal bleu"*/
int main(int argc, char* argv[])
{
    char cNomImgLue[250];
    int nH, nW, nR, nG, nB, nTaille;
    
    if (argc != 2) 
    {
        printf("Usage: ImageIn.pgm \n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s",cNomImgLue) ;
    
    OCTET *ImgIn;

        
    lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    
    int nTaille3 = nTaille * 3;
    allocation_tableau(ImgIn, OCTET, nTaille3);
    lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
 


    int tabOccR [256] ={0}; 
    int tabOccG [256] ={0}; 
    int tabOccB [256] ={0}; 

  


    for (int i=0; i < nTaille3; i+=3){
        

        nR = ImgIn[i];
        nG = ImgIn[i+1];
        nB = ImgIn[i+2];

        tabOccR[nR]++;
        
        tabOccG[nG]++;
        
        tabOccB[nB]++;
    }


    for (int i=0; i<256; i++){ 
        printf("%d %d %d %d \n", i, tabOccR[i], tabOccG[i],  tabOccB[i]);
       
    }

}