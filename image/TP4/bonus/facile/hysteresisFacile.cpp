// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char *argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille, SH, SB;

    if (argc != 5)
    {
        printf("Usage: ImageIn.pgm ImageOut.pgm SB SH \n");
        exit(1);
    }

    sscanf(argv[1], "%s", cNomImgLue);
    sscanf(argv[2], "%s", cNomImgEcrite);
    sscanf(argv[3], "%d", &SB);
    sscanf(argv[4], "%d", &SH);

    OCTET *ImgIn, *ImgOut;
   
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
  
    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);
    
    
    /*Hystérésis : Si les pixel sont au dessus de SH, on sait qu'ils nous interesse
    Si les pixel sont en dessous du SB alors on en a pas vraiement besoin 
    Suite à ce premier seuillage, va permettre de conserver les parties dont on est sur de vouloir garder
    

    2em seuillage : parmis ceux qui restent, on les prends ou non*/

    bool modif = true;

    while(modif) {
       
        
        for (int i = 1; i < nH-1; i++){
            for (int j = 1; j < nW-1; j++){
                // Si un pixel est encore non-classé mais que tous ses voisins sont à 0, il n'a aucune chance de passer à 255, donc on l'affecte à 0.
                if (ImgIn[indiceImg(i,j, nW, nH)] != 0 && ImgIn[indiceImg(i,j, nW, nH)] != 255 && 
                    (ImgIn[indiceImg(i-1,j, nW, nH)] == 0
                    && ImgIn[indiceImg(i+1,j, nW, nH)] == 0
                    && ImgIn[indiceImg(i,j-1, nW, nH)] == 0
                    && ImgIn[indiceImg(i,j+1, nW, nH)] == 0))
                {
                    ImgOut[indiceImg(i,j, nW, nH)] = 0;
                  
                }

                else if (SB < ImgIn[indiceImg(i,j, nW, nH)] && ImgIn[indiceImg(i,j, nW, nH)] < SH && 
                    (ImgIn[indiceImg(i-1,j, nW, nH)] == 255
                    || ImgIn[indiceImg(i+1,j, nW, nH)] == 255
                    || ImgIn[indiceImg(i,j-1, nW, nH)] == 255
                    || ImgIn[indiceImg(i,j+1, nW, nH)] == 255)
                    || ImgIn[indiceImg(i,j, nW, nH)] == 255)
                {
                    ImgOut[indiceImg(i,j, nW, nH)] = 255;
                    

                }
                //si un pixel non-classé est voisin d'un autre pixel non-classé, le deuxième a une chance de passer à 255, et de "transmettre son 255" au pixel courant. 
                //On décide donc d'attendre le prochain passage du traitement d'image avant de décider.
                else if (ImgIn[indiceImg(i,j, nW, nH)] != 0 && ImgIn[indiceImg(i,j, nW, nH)] != 255 &&
                    ((ImgIn[indiceImg(i-1,j, nW, nH)] != 0 && ImgIn[indiceImg(i-1,j, nW, nH)] != 255 )
                        || (ImgIn[indiceImg(i+1,j, nW, nH)] != 0 && ImgIn[indiceImg(i+1,j, nW, nH)] != 255)
                        || (ImgIn[indiceImg(i,j-1, nW, nH)] != 0 && ImgIn[indiceImg(i,j-1, nW, nH)] != 255)
                        || (ImgIn[indiceImg(i,j+1, nW, nH)] != 0 && ImgIn[indiceImg(i,j+1, nW, nH)] != 255)))
                {
                    // On ne fait rien, on attend le prochain passage
                    continue;

                }
                // Si aucun pixel n'est dans ce cas, c'est qu'il ne reste plus de pixel non-classé, on peut s'arrêter.
                else {
                    modif = false;
                }
            }
        }
    }

    

    ecrire_image_pgm(cNomImgEcrite, ImgOut, nH, nW);
    free(ImgIn);
    

    return 1;
}
