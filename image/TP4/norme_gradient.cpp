
#include <stdio.h>
#include "image_ppm.h"
#include <cmath>

int main(int argc, char *argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille;

    if (argc != 3)
    {
        printf("Usage: ImageIn.pgm ImageOut.pgm \n");
        exit(1);
    }

    sscanf(argv[1], "%s", cNomImgLue);
    sscanf(argv[2], "%s", cNomImgEcrite);

    OCTET *ImgIn, *ImgOut;

    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;

    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);

    /*si la différence entre les deux pixel est forte alors 
    tu as une grande norme et du coup on a du blanc 
    sinon on a du noir*/

    /*les bord pas traité prend les valeurs du bord du debut. du coup on met les bord en noir*/

    for(int j = 0; j < nW; j++){
        ImgOut[indiceImg(0,j, nW, nH)] = 0; 
        ImgOut[indiceImg(nH-1,j, nW, nH)] = 0;
    }

    for(int i = 0; i < nH; i++){
        ImgOut[indiceImg(i,0, nW, nH)] = 0; 
        ImgOut[indiceImg(i,nW-1, nW, nH)] = 0;
    }

    for (int i = 1; i < nH-1; i++){
        for (int j = 1; j < nW-1; j++){
            
            int gradHorizontal = ImgIn[indiceImg(i+1, j, nW, nH)] - ImgIn[indiceImg(i, j, nW, nH)]; 
            int gradVertical = ImgIn[indiceImg(i, j+1, nW, nH)] - ImgIn[indiceImg(i, j, nW, nH)]; 
            int m = sqrt(gradVertical *gradVertical + gradHorizontal * gradHorizontal); 
            ImgOut[indiceImg(i,j, nW, nH)] = m; 

        }
    }


    ecrire_image_pgm(cNomImgEcrite, ImgOut, nH, nW);
    free(ImgIn);
    

    return 1;
}
