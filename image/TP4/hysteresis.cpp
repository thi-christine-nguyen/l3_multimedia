// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char *argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille, SH, SB;

    if (argc != 5)
    {
        printf("Usage: ImageIn.pgm ImageOut.pgm SB SH \n");
        exit(1);
    }

    sscanf(argv[1], "%s", cNomImgLue);
    sscanf(argv[2], "%s", cNomImgEcrite);
    sscanf(argv[3], "%d", &SB);
    sscanf(argv[4], "%d", &SH);

    OCTET *ImgIn, *ImgOut, *ImgBe;
   
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
  
    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);
    allocation_tableau(ImgBe, OCTET, nTaille);
    
    /*Hystérésis : Si les pixel sont au dessus de SH, on sait qu'ils nous interesse
    Si les pixel sont en dessous du SB alors on en a pas vraiement besoin 
    Suite à ce premier seuillage, va permettre de conserver les parties dont on est sur de vouloir garder
    
    1er seuillage : on enlève les pixels parasites
    2em seuillage : parmis ceux qui restent, on les prends ou non*/

    for (int i = 1; i < nH-1; i++){
        for (int j = 1; j < nW-1; j++)
        {
            if (ImgIn[indiceImg(i,j, nW, nH)] <= SB)
                ImgBe[indiceImg(i,j, nW, nH)] = 0;
            else if (ImgIn[indiceImg(i,j, nW, nH)] >= SH)
                ImgBe[indiceImg(i,j, nW, nH)] = 255;
        }

    }

    for (int i = 1; i < nH-1; i++){
        for (int j = 1; j < nW-1; j++)
        {
            if (SB < ImgBe[indiceImg(i,j, nW, nH)] && ImgBe[indiceImg(i,j, nW, nH)] < SH && 
                (ImgBe[indiceImg(i-1,j, nW, nH)] == 255
                || ImgBe[indiceImg(i+1,j, nW, nH)] == 255
                || ImgBe[indiceImg(i,j-1, nW, nH)] == 255
                || ImgBe[indiceImg(i,j+1, nW, nH)] == 255)
                || ImgBe[indiceImg(i,j, nW, nH)] == 255)
            {
                ImgOut[indiceImg(i,j, nW, nH)] = 255;

            }else{
                ImgOut[indiceImg(i,j,nW,nH)] = 0; 
            }
        }

    }

    ecrire_image_pgm(cNomImgEcrite, ImgOut, nH, nW);
    free(ImgIn);
    free(ImgOut); 

    return 1;
}
