// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char *argv[])
{
    char cNomImgLueY[250], cNomImgLueCb[250] , cNomImgLueCr[250] , cNomImgEcrite[250];
    int nH, nW, nTaille;

    if (argc != 5) 
    {
       printf("Usage: Y.pgm Cb.pgm Cr.pgm ImageOut.ppm \n"); 
       exit (1) ;
    }

    sscanf (argv[1],"%s",cNomImgLueY) ;
    sscanf (argv[2],"%s",cNomImgLueCb);
    sscanf (argv[3],"%s",cNomImgLueCr);
    sscanf (argv[4],"%s",cNomImgEcrite);
    



    OCTET *ImgOut, *Y, *Cb, *Cr;

    lire_nb_lignes_colonnes_image_pgm(cNomImgLueY, &nH, &nW);
    lire_nb_lignes_colonnes_image_pgm(cNomImgLueCb, &nH, &nW);
    lire_nb_lignes_colonnes_image_pgm(cNomImgLueCr, &nH, &nW);
    nTaille = nH * nW;

    int nTaille3 = nTaille * 3;
    allocation_tableau(Y, OCTET, nTaille);
    lire_image_pgm(cNomImgLueY, Y, nH * nW);

    allocation_tableau(Cb, OCTET, nTaille);
    lire_image_pgm(cNomImgLueCb, Cb, nH * nW);

    allocation_tableau(Cr, OCTET, nTaille);
    lire_image_pgm(cNomImgLueCr, Cr, nH * nW);
    



    allocation_tableau(ImgOut, OCTET, nTaille3);
  


    int j = 0; 
    
    for (int i=0; i < nTaille3; i+=3)

    {   
        


        ImgOut[i] = clamp(Y[j]+ 1.402*(Cr[j]-128), 0, 255); 
        ImgOut[i+2] = clamp(Y[j] - 0.34414 * (Cb[j]-128) - 0.714414 * (Cr[j]-128), 0, 255);
        ImgOut[i+1] = clamp(Y[j] + 1.772 * (Cb[j]-128), 0, 255); 


        j++; 
        
    }

    ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
   
    
    free(Y);
    free(Cb);
    free(Cr);
    free(ImgOut);
  

    return 1;
}

//Le Y influencie la luminosité 
//Cb (complément en bleu) met en blanc le bleu
//Cr (complément en rouge) met en blanc le rouge