// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char *argv[])
{
    char cNomImgLueY[250], cNomImgLueCb[250] , cNomImgLueCr[250] , cNomImgEcrite[250],cNomImgEcriteR[250], cNomImgEcriteG[250], cNomImgEcriteB[250];
    int nH, nW, nTaille;

    if (argc != 8) 
    {
       printf("Usage: Y.pgm Cb.pgm Cr.pgm ImageOut.ppm ImageR.ppm ImageG.ppm ImageB.ppm\n"); 
       exit (1) ;
    }

    sscanf (argv[1],"%s",cNomImgLueY) ;
    sscanf (argv[2],"%s",cNomImgLueCb);
    sscanf (argv[3],"%s",cNomImgLueCr);
    sscanf (argv[4],"%s",cNomImgEcrite);
    sscanf (argv[5],"%s",cNomImgEcriteR);
    sscanf (argv[6],"%s",cNomImgEcriteG);
    sscanf (argv[7],"%s",cNomImgEcriteB);



    OCTET *ImgOut, *Y, *Cb, *Cr, *ImgR, *ImgG, *ImgB;

    lire_nb_lignes_colonnes_image_pgm(cNomImgLueY, &nH, &nW);
    lire_nb_lignes_colonnes_image_pgm(cNomImgLueCb, &nH, &nW);
    lire_nb_lignes_colonnes_image_pgm(cNomImgLueCr, &nH, &nW);
    nTaille = nH * nW;

    int nTaille3 = nTaille * 3;
    allocation_tableau(Y, OCTET, nTaille);
    lire_image_pgm(cNomImgLueY, Y, nH * nW);

    allocation_tableau(Cb, OCTET, nTaille);
    lire_image_pgm(cNomImgLueCb, Cb, nH * nW);

    allocation_tableau(Cr, OCTET, nTaille);
    lire_image_pgm(cNomImgLueCr, Cr, nH * nW);
    



    allocation_tableau(ImgOut, OCTET, nTaille3);
    allocation_tableau(ImgR, OCTET, nTaille3);
    allocation_tableau(ImgG, OCTET, nTaille3);
    allocation_tableau(ImgB, OCTET, nTaille3);


    int j = 0; 
    
    for (int i=0; i < nTaille3; i+=3)
    {   
        ImgOut[i] = Y[j] + 1.402*(Cr[j]-128); 
        ImgOut[i+1] = Y[j] - 0.34414 * (Cb[j]-128) - 0.714414 * (Cr[j]-128);
        ImgOut[i+2] = Y[j] + 1.772 * (Cb[j]-128); 

        ImgR[i] = ImgOut[i]; 
        ImgG[i+1] = ImgOut[i+1]; 
        ImgB[i+2] = ImgOut[i+2]; 



        j++; 
        
    }

    ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
    ecrire_image_ppm(cNomImgEcriteR, ImgR,  nH, nW);
    ecrire_image_ppm(cNomImgEcriteG, ImgG,  nH, nW);
    ecrire_image_ppm(cNomImgEcriteB, ImgB,  nH, nW);
    
    free(Y);
    free(Cb);
    free(Cr);
    free(ImgOut);
    free(ImgR);
    free(ImgB);


    return 1;
}

//Le Y influencie la luminosité 
//Cb (complément en bleu) met en blanc le bleu
//Cr (complément en rouge) met en blanc le rouge