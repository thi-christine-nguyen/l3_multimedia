// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char *argv[])
{
    char cNomImgLue[250], cNomImgLue2[250];
    int nH, nW, nTaille;

    if (argc != 3)
    {
        printf("Usage: ImageIn.pgm ImageIn2.pgm \n");
        exit(1);
    }

    sscanf(argv[1], "%s", cNomImgLue);
    sscanf(argv[2], "%s", cNomImgLue2);
    
    OCTET *ImgIn, *ImgIn2;

    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue2, &nH, &nW);
    nTaille = nH * nW;

    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);

    allocation_tableau(ImgIn2, OCTET, nTaille);
    lire_image_pgm(cNomImgLue2, ImgIn2, nH * nW);
    

    //   for (int i=0; i < nTaille; i++)
    // {
    //  if ( ImgIn[i] < S) ImgOut[i]=0; else ImgOut[i]=255;
    //  }

    int tab[nTaille]; 

    for (int i = 0; i < nH; i++){
        for (int j = 0; j < nW; j++)
        {
            tab[indiceImg(i,j, nW, nH)] = (ImgIn[indiceImg(i,j, nW, nH)] - ImgIn2[indiceImg(i,j, nW, nH)]) * (ImgIn[indiceImg(i,j, nW, nH)] - ImgIn2[indiceImg(i,j, nW, nH)]);
        }

    }


    int eqm = 0; 
    for (int i = 0; i < nH; i++){
        for (int j = 0; j < nW; j++)
        {
            eqm += tab[indiceImg(i,j, nW, nH)];
        }

    }

    eqm = eqm/nTaille;

    printf("%d \n", eqm);

    
    return eqm;
}
