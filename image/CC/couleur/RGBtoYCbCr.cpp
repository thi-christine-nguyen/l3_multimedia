// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char *argv[])
{
    char cNomImgLue[250], cNomImgEcriteY[250] , cNomImgEcriteCb[250] , cNomImgEcriteCr[250];
    int nH, nW, nTaille;

    if (argc != 5) 
    {
       printf("Usage: ImageIn.ppm Y.pgm Cb.pgm Cr.pgm \n"); 
       exit (1) ;
    }

    sscanf (argv[1],"%s",cNomImgLue) ;
    sscanf (argv[2],"%s",cNomImgEcriteY);
    sscanf (argv[3],"%s",cNomImgEcriteCb);
    sscanf (argv[4],"%s",cNomImgEcriteCr);



    OCTET *ImgIn, *Y, *Cb, *Cr;

    lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;

    int nTaille3 = nTaille * 3;
    allocation_tableau(ImgIn, OCTET, nTaille3);
    lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(Y, OCTET, nTaille);
    allocation_tableau(Cb, OCTET, nTaille);
    allocation_tableau(Cr, OCTET, nTaille);


    int j = 0; 
    
    for (int i=0; i < nTaille3; i+=3)
    {

        Y[j] =(0.299*ImgIn[i] + 0.587*ImgIn[i+1] + 0.114*ImgIn[i+2]);
        Cb[j] =(-0.1687*ImgIn[i] - 0.3313*ImgIn[i+1] + 0.5*ImgIn[i+2] + 128);
        Cr[j] =(0.5*ImgIn[i] - 0.4187*ImgIn[i+1] - 0.0813*ImgIn[i+2]+128); 
        j++; 
        
    }

    ecrire_image_pgm(cNomImgEcriteY, Y,  nH, nW);
    ecrire_image_pgm(cNomImgEcriteCb, Cb,  nH, nW);
    ecrire_image_pgm(cNomImgEcriteCr, Cr,  nH, nW);
    free(ImgIn);

    return 1;
}

//Le Y influencie la luminosité 
//Cb (complément en bleu) met en blanc le bleu
//Cr (complément en rouge) met en blanc le rouge