// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille;
    
    if (argc != 3) 
    {
        printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s",cNomImgLue) ;
    sscanf (argv[2],"%s",cNomImgEcrite);

    OCTET *ImgIn, *ImgOut;
    
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    
    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);

    for(int i =0; i<nH; i++){
        ImgOut[indiceImg(i, 0, nW, nH)] = ImgIn[indiceImg(i, 0, nW, nH)];
        ImgOut[indiceImg(i, nW-1, nW, nH)] = ImgIn[indiceImg(i, nW-1, nW, nH)];
    }



    for(int j =0; j<nW; j++){
        ImgOut[indiceImg(0, j, nW, nH)] = ImgIn[indiceImg(0, j, nW, nH)];
        ImgOut[indiceImg(nH-1, j, nW, nH)] = ImgIn[indiceImg(nH-1, j, nW, nH)];
    }


    for (int i=1; i < nH-1; i++)
        for (int j=1; j < nW-1; j++)
        {
            //p_out(i,j)= ( p(i,j)+p(i-1,j)+p(i+1,j)+p(i,j-1)+p(i,j+1) )/5

            ImgOut[indiceImg(i, j, nW, nH)] =   (ImgIn[indiceImg(i+1, j, nW, nH)] + 
                                                ImgIn[indiceImg(i-1, j, nW, nH)] + 
                                                ImgIn[indiceImg(i, j+1, nW, nH)] + 
                                                ImgIn[indiceImg(i, j-1, nW, nH)] + 
                                                ImgIn[indiceImg(i, j, nW, nH)])/5;
        }

    ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
    free(ImgIn); free(ImgOut);

    return 1;
}
