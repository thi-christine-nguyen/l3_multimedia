#include <stdio.h>
#include <stdlib.h>
#include "image_ppm.h"

int main(int argc, char *argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille, nbSeuils;

    if (argc < 4)
    {
        printf("Usage: %s ImageIn.pgm ImageOut.pgm nbSeuils\n", argv[0]);
        exit(1);
    }

    sscanf(argv[1], "%s", cNomImgLue);
    sscanf(argv[2], "%s", cNomImgEcrite);
    sscanf(argv[3], "%d", &nbSeuils);

    OCTET *ImgIn, *ImgOut;

    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;

    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);

    for (int i = 0; i < nTaille; i++)
    {

        /*si l'image a 3 seuils, la largeur de chaque intervalle sera de (255 / (3 + 1)) = 63. 
        Si pixel = 120, alors (120 / 63) = 1
        On fait * 63 ce qui donne 63 (arrondi)
        */

        int largeurIntervalle = 255 / (nbSeuils + 1); // Calcule la largeur de chaque intervalle
        int indexIntervalle = ImgIn[i] / largeurIntervalle; // Calcule l'indice de l'intervalle correspondant au pixel
        int seuil = indexIntervalle * largeurIntervalle; // Calcule le seuil inférieur associé au pixel

        ImgOut[i] = seuil;
    }

    ecrire_image_pgm(cNomImgEcrite, ImgOut, nH, nW);
    free(ImgIn);
    free(ImgOut);

    return 1;
}
