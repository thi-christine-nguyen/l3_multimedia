// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char *argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille, S1, S2;

    if (argc != 5)

    {
        printf("Usage: ImageIn.pgm ImageOut.pgm Seuil1 Seuil2 \n");
        exit(1);
    }

    sscanf(argv[1], "%s", cNomImgLue);
    sscanf(argv[2], "%s", cNomImgEcrite);
    sscanf(argv[3], "%d", &S1);
    sscanf(argv[4], "%d", &S2);

    OCTET *ImgIn, *ImgOut;

    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;

    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);

    //   for (int i=0; i < nTaille; i++)
    // {
    //  if ( ImgIn[i] < S) ImgOut[i]=0; else ImgOut[i]=255;
    //  }

    for (int i = 0; i < nH; i++)
    {
        for (int j = 0; j < nW; j++)
        {
            if (ImgIn[i * nW + j] < S1)
            {
                ImgOut[i * nW + j] = 0; // noir
            }

            else if (ImgIn[i * nW + j] > S2)
            {
                ImgOut[i * nW + j] = 255; 
            }
            else {
                
                if (S1 != S2) { //pour pas que t soit négatif
                    int t = (ImgIn[i * nW + j] - S1) * 255 / (S2 - S1);
                    //Interpolation y1 :
                    int y1 = pow(t / 255, 3) * 255; 

                    //Interpolation y2 : 
                    int y2  = (3 * pow(t, 2) - 2 * pow(t, 3)) * 255;

                    // Interpolation avec la fonction sin() :
                    int y3 = sin(t * (M_PI / 2)) * 255;

                    ImgOut[i * nW + j] = y3;
                } else {
                    ImgOut[i * nW + j] = 0;
                }



                //Interpolation y2 : 
                //int y2  = (3 * pow(t, 2) - 2 * pow(t, 3)) * 255;
                

                // Interpolation avec la fonction sin() :
                //int y3 = sin(t * (M_PI / 2)) * 255;
                //ImgOut[i * nW + j] = (int)y1;
            }
        }
    }

    ecrire_image_pgm(cNomImgEcrite, ImgOut, nH, nW);
    free(ImgIn);
    free(ImgOut);

    return 1;
}
