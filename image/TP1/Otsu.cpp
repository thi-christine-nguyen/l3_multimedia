
#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille;
    int sum, u1, u2, q1, q2, sumB, threshold, var_max = 0;
    
    if (argc != 3) 
    {
        printf("Usage: ImageIn.pgm ImageOut.pgm\n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s",cNomImgLue) ;
    sscanf (argv[2],"%s",cNomImgEcrite);
    
    OCTET *ImgIn, *ImgOut;
   
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    
    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);

    int histo [256];

    for (int i=0; i<256; i++){ 
        histo[i]=0;
    }

    for (int i=0; i < nH; i++){ 
        for (int j=0; j < nW; j++)
        {
            int val=ImgIn[i*nW+j];
            histo[val]++;
        
        }
    } 

    for (int i = 0; i < 256; i++){
        sum += i*histo[i];
    }

    for(int t = 0; t < 256; t++){
        q1 += histo[t]; 
        if(q1 == 0){
            continue;
        }
        q2=nTaille-q1; 
        if(q2==0){
            break;
        } 
        

        sumB += t*histo[t];
        
        u1 = sumB/q1; 

        
        u2 = (sum-sumB)/q2; 
        
        int v = q1 * q2 * (u1-u2) * (u1-u2);
        
        
        if(v > var_max){
            threshold = t; 
            var_max = v;
        }

    }


    for (int i=0; i < nH; i++){ 
        for (int j=0; j < nW; j++){ 
            if(ImgIn[i*nW+j] > threshold){
                ImgOut[i*nW+j] = 255;
            }else{
                ImgOut[i*nW+j] = 0; 
            }
        }
    } 

    printf("seuil =  %d", threshold); 


    ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
    free(ImgIn); free(ImgOut);

    return 1;
    
}

