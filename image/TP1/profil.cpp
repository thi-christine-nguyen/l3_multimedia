
#include <stdio.h>
#include "image_ppm.h"

//plot 'profilZebreCol.dat' with linespoints pointtype 7 

int main(int argc, char* argv[])
{
    char cNomImgLue[250];
    int nH, nW, nTaille, lc, indice;
    
    if (argc != 4) 
    {
        printf("Usage: ImageIn.pgm, ligne(1) ou colonne(0), indice \n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s",cNomImgLue) ;
    sscanf (argv[2],"%d",&lc);
    sscanf (argv[3],"%d",&indice);
    
    OCTET *ImgIn;

    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;

    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);

   

    int tabOcc;


    if(lc == 0){ //colonne
        int tabOcc [nH];
        for(int i=0; i < nH; i++){
            int val=ImgIn[indiceImg(i, indice, nW, nH)];
            tabOcc[i] = val;
        }
        for (int i=0; i<nH; i++){ 
            printf("%d %d\n",i, tabOcc[i]);
        }
    }

    if(lc == 1){//ligne
        int tabOcc [nW];
        for(int j=0; j < nW; j++){
            int val=ImgIn[indiceImg(indice, j, nW, nH)];
            tabOcc[j] = val;
        }
        for (int i=0; i<nW; i++){ 
            printf("%d %d\n",i, tabOcc[i]);
        }
    }


   

    
}