#include "Mesh.h"
#include <iostream>
#include <fstream>

Vec3 Mesh::scalarToRGB( float scalar_value ) const //Scalar_value ∈ [0, 1]
{
    Vec3 rgb;
    float H = scalar_value*300., S = 1., V = 1.,
            P, Q, T,
            fract;

    (H == 360.)?(H = 0.):(H /= 60.);
    fract = H - floor(H);

    P = V*(1. - S);
    Q = V*(1. - S*fract);
    T = V*(1. - S*(1. - fract));

    if      (0. <= H && H < 1.)
        rgb = Vec3( V,  T,  P);
    else if (1. <= H && H < 2.)
        rgb = Vec3( Q,  V,  P);
    else if (2. <= H && H < 3.)
        rgb = Vec3( P,  V,  T);
    else if (3. <= H && H < 4.)
        rgb = Vec3( P,  Q,  V);
    else if (4. <= H && H < 5.)
        rgb = Vec3( T,  P,  V);
    else if (5. <= H && H < 6.)
        rgb = Vec3( V,  P,  Q);
    else
        rgb = Vec3( 0.,  0.,  0.);

    return rgb;
}

void Mesh::computeSkinningWeights( Skeleton & skeleton ) {
    //---------------------------------------------------//
    //---------------------------------------------------//
    // code to change :

    // Indications:
    // you should compute weights for each vertex w.r.t. the skeleton bones
    // so each vertex will have B weights (B = number of bones)
    //Don't forget to normalize weights
    // these weights shoud be stored in vertex.weights:

    for( unsigned int i = 0 ; i < vertices.size() ; ++i ) {
        MeshVertex & vertex = vertices[ i ];
        Vec3 C = vertex.position;
        vertex.weights.resize(skeleton.bones.size());
        float poids_total = 0;
        for (int iBone = 0; iBone < skeleton.bones.size(); iBone++) {
            Bone& bone = skeleton.bones[iBone];
            int art_0 = bone.joints[0];
            int art_1 = bone.joints[1];

            Vec3 A = skeleton.articulations[art_0].position;
            Vec3 B = skeleton.articulations[art_1].position;

            // Calcul des vecteurs utilisés dans le calcul de distance point-droite
            Vec3 AC = C - A;
            Vec3 AB = B - A;

            // Calcul de la distance:
            // Le point C doit être projeté sur la droite AB, on l'appellera C'
            // 1. On applique la projection avec le dot product, ce qui nous donne la distance entre le point A et le point projeté
            // 2. On limite cette distance entre 0 et ||AB|| pour que ce point soit projeté sur le SEGMENT (et non la droite)
            // 3. On crée le point C' à cette distance de A, le long de la droite AB
            // 4. On calcule la distance ||CC'||

            // 1.
            float distance_AC_prime = (Vec3::dot(AB, AC)) / AB.length();

            // 2.
            if (distance_AC_prime < 0)
                distance_AC_prime = 0;
            if (distance_AC_prime > AB.length())
                distance_AC_prime = AB.length();

            // 3.
            Vec3 u = AB / AB.length();
            Vec3 C_prime = A + distance_AC_prime* u;

            // 4.
            float dist_ij = (C - C_prime).length();

            // On reprend exactement la formule du cours pour calculer un poids. Faites varier "n" pour comprendre à quoi il sert
            float n = 2;
            float w_ij = std::pow(1.0 / dist_ij, 3);

            // On somme la valeur des poids pour pouvoir les normaliser juste après
            poids_total += w_ij;
            vertex.weights[iBone] = w_ij;
        }
        // Normalisation des poids pour que la somme soit 1
        for (int iBone = 0; iBone < skeleton.bones.size(); iBone++) {
            vertex.weights[iBone] /= poids_total;
        }

    }
    //---------------------------------------------------//
    //---------------------------------------------------//
    //---------------------------------------------------//
}

void Mesh::draw( int displayed_bone ) const {

    glEnable(GL_LIGHTING);
    glBegin (GL_TRIANGLES);
    for (unsigned int i = 0; i < triangles.size (); i++)
        for (unsigned int j = 0; j < 3; j++) {
            const MeshVertex & v = vertices[triangles[i].v[j]];
            if( displayed_bone >= 0 && v.weights.size() > 0 ){
                // code to change :

                // Indications:
                //Call the function scalarToRGB so that you get the same coloring as slide 51
                //Update the color from the Vec3 resulting color

                // Pas sur qu'il y ait besoin d'explications ici
                // J'ai modifie un tout petit peu la fonction scalarToRGB.
                // Precedemment, un scalaire proche de 0 et un scalaire proche de 1 s'affichent en rouge,
                // J'ai limite la composante H de l'espace HSV pour eviter que ca se reproduise
                Vec3 color = this->scalarToRGB(v.weights[displayed_bone]);
                glColor3f(color[0], color[1], color[2]);

            }
            glNormal3f (v.normal[0], v.normal[1], v.normal[2]);
            glVertex3f (v.position[0], v.position[1], v.position[2]);
        }
    glEnd ();
}

void Mesh::drawTransformedMesh( SkeletonTransformation & transfo ) const {
    std::vector< Vec3 > new_positions( vertices.size() );

    //---------------------------------------------------//
    //---------------------------------------------------//
    // code to change :
    for( unsigned int i = 0 ; i < vertices.size() ; ++i ) {
        Vec3 p = vertices[i].position;

        // Indications:
        // you should use the skinning weights to blend the transformations of the vertex position by the bones.
        // to update the position use the weight and the bone transformation
        // for each bone p'=R*p+t

        // On veut calculer la position de notre vertex d'après les matrices de transformation des os qui lui sont associés.
        // Imaginons que le vertex soit associé à un unique os,
        // ca doit vous paraitre logique que la transformation à appliquer au vertex soit identique à celui de l'os
        // ( p' = R * p + t )
        // Mais si on veut lui faire suivre 2 os différents (supposons que les poids sont de 0.5 et 0.5), on devrait faire la moyenne des transformations
        // ( p1' = R1 * p + t1  et p2' = R2 * p + t2 )
        // ( alors p' = p1' * 0.5 + p2' * 0.5 )
        // Enfin, supposons que les poids soit "u" et "v" (avec u + v = 1, bien sur!)
        // ( p' = p1' * u + p2' * v )
        // On garde la meme idee pour un nombre quelconque d'os!

        // p' = p1' * w1 + p2' * w2 + p3' * w3 + ... + pn' * wn
        Vec3 newPos = Vec3(0, 0, 0);
        for (unsigned int iBone = 0; iBone < vertices[i].weights.size(); iBone++) {
            // On recupere la transformation (rotation R et translation T
            Mat3 R = transfo.bone_transformations[iBone].world_space_rotation;
            Vec3 t = transfo.bone_transformations[iBone].world_space_translation;
            float w = vertices[i].weights[iBone];

            // On applique la transformation depuis le point initial "p" et on "l'additionne" à la position finale p'
            newPos += w * (R * p + t);
        }
        // Enfin, on applique le resultat dans la liste finale
        new_positions[ i ] = newPos;

    }
    //---------------------------------------------------//
    //---------------------------------------------------//
    //---------------------------------------------------//

    glEnable(GL_LIGHTING);
    glBegin (GL_TRIANGLES);
    for (unsigned int i = 0; i < triangles.size (); i++)
        for (unsigned int j = 0; j < 3; j++) {
            const MeshVertex & v = vertices[triangles[i].v[j]];
            Vec3 p = new_positions[ triangles[i].v[j] ];
            glNormal3f (v.normal[0], v.normal[1], v.normal[2]);
            glVertex3f (p[0], p[1], p[2]);
        }
    glEnd ();
}

void Mesh::loadOFF (const std::string & filename) {
    std::ifstream in (filename.c_str ());
    if (!in)
        exit (EXIT_FAILURE);
    std::string offString;
    unsigned int sizeV, sizeT, tmp;
    in >> offString >> sizeV >> sizeT >> tmp;
    vertices.resize (sizeV);
    triangles.resize (sizeT);
    for (unsigned int i = 0; i < sizeV; i++)
        in >> vertices[i].position;
    int s;
    for (unsigned int i = 0; i < sizeT; i++) {
        in >> s;
        for (unsigned int j = 0; j < 3; j++)
            in >> triangles[i].v[j];
    }
    in.close ();
    recomputeNormals ();
}

void Mesh::recomputeNormals () {
    for (unsigned int i = 0; i < vertices.size (); i++)
        vertices[i].normal = Vec3 (0.0, 0.0, 0.0);
    for (unsigned int i = 0; i < triangles.size (); i++) {
        Vec3 e01 = vertices[triangles[i].v[1]].position -  vertices[triangles[i].v[0]].position;
        Vec3 e02 = vertices[triangles[i].v[2]].position -  vertices[triangles[i].v[0]].position;
        Vec3 n = Vec3::cross (e01, e02);
        n.normalize ();
        for (unsigned int j = 0; j < 3; j++)
            vertices[triangles[i].v[j]].normal += n;
    }
    for (unsigned int i = 0; i < vertices.size (); i++)
        vertices[i].normal.normalize ();
}
