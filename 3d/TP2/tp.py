import math

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from OpenGL.GL import shaders, GL_VERTEX_SHADER, GL_FRAGMENT_SHADER
import pygame as pg
from pygame.locals import *

EXIT_SUCCESS = 1
EXIT_FAILURE = -1


def printOpenGLError():
    retCode = 0
    glErr = glGetError()
    while glErr != GL_NO_ERROR:
        printf("glError in file %s @ line %d: %s\n", file, line, gluErrorString(glErr))
        retCode = 1
        glErr = glGetError()
    return retCode


def printProgramInfoLog(shaderProgram: GLuint):
    # infologLength: int = 0
    charsWritten: int = 0
    infoLog = ""

    printOpenGLError()
    infologLength = glGetShaderiv(shaderProgram, GL_INFO_LOG_LENGTH)
    printOpenGLError()

    if infologLength > 0:
        infoLog = ""
    if infoLog == NULL:
        printf("ERROR: Could not allocate InfoLog buffer\n")
        exit(1)
    glGetShaderInfoLog(shader, infologLength, charsWritten, infoLog)
    printOpenGLError()


class PhongShader:
    def __init__(self, vertexShaderFilename: str, fragmentShaderFilename: str):
        self.ambientRefLocation: GLuint = -1
        self.levelsLocation: GLuint = -1
        self.diffuseRefLocation: GLuint = -1
        self.specularRefLocation: GLuint = -1
        self.shininessLocation: GLuint = -1
        self.rougeLocation: GLuint = -1
        self.vertLocation: GLuint = -1
        self.bleuLocation: GLuint = -1
        self.vertexShader: GLuint = -1
        self.fragmentShader: GLuint = -1
        self.shaderProgram: GLuint = -1
        self._init(vertexShaderFilename, fragmentShaderFilename)

    def setLevels(self, s: float):
        glUniform1f(self.levelsLocation, s)

    def setAmbientRef(self, s: float):
        glUniform1f(self.ambientRefLocation, s)

    def setDiffuseRef(self, s: float):
        glUniform1f(self.diffuseRefLocation, s)

    def setSpecularRef(self, s: float):
        glUniform1f(self.specularRefLocation, s)

    def setShininess(self, s: float):
        glUniform1f(self.shininessLocation, s)
    
    def setRouge(self, s: float):
        glUniform1f(self.rougeLocation, s)

    def setVert(self, s: float):
        glUniform1f(self.vertLocation, s)

    def setBleu(self, s: float):
        glUniform1f(self.bleuLocation, s)

    def _init(self, vertexShaderFilename: str, fragmentShaderFilename: str):
        self.loadFromFile(vertexShaderFilename, fragmentShaderFilename)
        self.bind()
        self.ambientRefLocation = self.getUniLoc("ambientRef")
        self.levelsLocation = self.getUniLoc("levels")
        self.diffuseRefLocation = self.getUniLoc("diffuseRef")
        self.specularRefLocation = self.getUniLoc("specularRef")
        self.shininessLocation = self.getUniLoc("shininess")
        self.rougeLocation = self.getUniLoc("rouge")
        self.vertLocation = self.getUniLoc("vert")
        self.bleuLocation = self.getUniLoc("bleu")

    def loadFromFile(self, vertexShaderFilename: str, fragmentShaderFilename: str):
        vertexShaderContent = ""
        fragmentShaderContent = ""
        with open(vertexShaderFilename, "r", encoding="utf-8") as f:
            vertexShaderContent = "".join(f.readlines())
        with open(fragmentShaderFilename, "r", encoding="utf-8") as f:
            fragmentShaderContent = "".join(f.readlines())

        self.vertexShader = shaders.compileShader(vertexShaderContent, GL_VERTEX_SHADER)
        self.fragmentShader = shaders.compileShader(fragmentShaderContent, GL_FRAGMENT_SHADER)
        self.shaderProgram = shaders.compileProgram(self.vertexShader, self.fragmentShader)
        glLinkProgram(self.shaderProgram)
        printOpenGLError()
        linked: GLuint = 0
        linked = glGetProgramiv(self.shaderProgram, GL_LINK_STATUS, linked)
        # printProgramInfoLog(self.shaderProgram)
        if not linked:
            ShaderException("Error: Shaders not linked")

    def bind(self):
        glUseProgram(self.shaderProgram)

    def getUniLoc(self, name: str):
        loc = glGetUniformLocation(self.shaderProgram, name)
        return loc


def crossProduct(v0: 'Vec3Df', v1: 'Vec3Df') -> 'Vec3Df':
    return Vec3Df(
        v0.y * v1.z - v0.z * v1.y,
        v0.z * v1.x - v0.x * v1.z,
        v0.x * v1.y - v0.y * v1.x
    )


def dotProduct(v0: 'Vec3Df', v1: 'Vec3Df') -> float:
    return v0.x * v1.x + v0.y * v1.y + v0.z * v1.z


class Vec3Df:
    def __init__(self, x, y=None, z=None):
        if isinstance(x, Vec3Df):
            self.x = x.x
            self.y = x.y
            self.z = x.z
        else:
            self.x = x
            self.y = y
            self.z = z

    def norm2(self) -> float:
        return self.x ** 2 + self.y ** 2 + self.z ** 2

    def norm(self) -> float:
        return math.sqrt(self.norm2())

    def normalize(self):
        norm = self.norm()
        if norm != 0:
            self.x /= norm
            self.y /= norm
            self.z /= norm

    def __add__(self, other):
        return Vec3Df(self.x + other.x, self.y + other.y, self.z + other.z)

    def __sub__(self, other):
        return Vec3Df(self.x - other.x, self.y - other.y, self.z - other.z)

    def __mul__(self, scalar):
        return Vec3Df(self.x * scalar, self.y * scalar, self.z * scalar)


class Vertex:
    def __init__(self, pos: Vec3Df, normal: Vec3Df = Vec3Df(0, 0, 0)):
        self.pos = pos
        self.normal = normal

    def getPos(self):
        return self.pos

    def getNormal(self):
        return self.normal


class Triangle:
    def __init__(self, v0, v1, v2):
        self.vertices = [v0, v1, v2]

    def getVertex(self, i: int) -> int:
        return self.vertices[i]


class Mesh:
    def __init__(self, V=None, T=None):
        if V is None:
            V = []
        if T is None:
            T = []
        self.vertices: List[Vertex] = V
        self.triangles: List[Triangle] = T
        # self.normals: List[Vec3Df] = []
        self.triangleNormals: List[Vec3Df] = []

    def getVertices(self):
        return self.vertices

    def getTriangles(self):
        return self.triangles

    def computeTriangleNormals(self, normalsArray):  # List[Vec3Df]):
        for i in range(len(self.triangles)):
            t = self.triangles[i]
            e01 = Vec3Df(self.vertices[t.getVertex(1)].getPos() - self.vertices[t.getVertex(0)].getPos())
            e02 = Vec3Df(self.vertices[t.getVertex(2)].getPos() - self.vertices[t.getVertex(0)].getPos())
            n = Vec3Df(crossProduct(e01, e02))
            n.normalize()
            normalsArray.append(n)

    def recomputeSmoothVertexNormals(self, normWeight: int):
        self.computeTriangleNormals(self.triangleNormals)
        for it in self.vertices:
            it.normal = Vec3Df(0.0, 0.0, 0.0)
        for i, t in enumerate(self.triangles):
            for j in range(3):
                vj = self.vertices[t.getVertex(j)]
                w = 1.0  # uniform weights
                e0 = self.vertices[t.getVertex((j + 1) % 3)].getPos() - vj.getPos()
                e1 = self.vertices[t.getVertex((j + 2) % 3)].getPos() - vj.getPos()
                if (normWeight == 1):  # area weight
                    w = crossProduct(e0, e1).getLength() / 2.0
                elif (normWeight == 2):  # // angle weight
                    e0.normalize()
                    e1.normalize()
                    w = (2.0 - (dotProduct(e0, e1) + 1.0)) / 2.0

                if (w <= 0.0):
                    continue
                vj.normal = (vj.getNormal() + self.triangleNormals[i] * w)
        for v in self.vertices:
            v.normal.normalize()


window: GLuint = -1
SCREENWIDTH: int = 1024
SCREENHEIGHT: int = 768
# camera: Camera = None
mouseRotatePressed: bool = False
mouseMovePressed: bool = False
mouseZoomPressed: bool = False
lastX: int = 0
lastY: int = 0
lastZoom: int = 0
FPS: int = 0
fullScreen: bool = False

phongShader: PhongShader

mesh: Mesh = None
glID: GLuint = -1

levels : float = 4.0
ambientRef: float = 0.3
diffuseRef: float = 0.8
specularRef: float = 0.5
shininess: float = 16.0

rouge : float = 0.0
vert : float = 0.0
bleu : float = 0.0



Wire = 0
Phong = 1
Solid = 2
mode = Phong


def glNormalVec3Df(vec: Vec3Df):
    glNormal3f(vec.x, vec.y, vec.z)


def glVertexVec3Df(vec: Vec3Df):
    glVertex3f(vec.x, vec.y, vec.z)


def openOFF(filename: str, normWeight: int) -> Mesh:
    global mesh
    V: List[Vertex] = []
    T: List[Triangle] = []

    with open(filename, "r") as f:
        f.readline()
        [sizeV, sizeT, tmp] = f.readline().split(" ")
        sizeV = int(sizeV)
        sizeT = int(sizeT)
        for i in range(sizeV):
            vals = [float(coord) for coord in f.readline().split()]
            p = Vec3Df(vals[0], vals[1], vals[2])
            V.append(Vertex(p))

        for i in range(sizeT):
            indices = [int(index) for index in f.readline().split()]
            if indices:
                T.append(Triangle(indices[1], indices[2], indices[3]))

        mesh = Mesh(V, T)
        mesh.recomputeSmoothVertexNormals(normWeight)
        return mesh


def glDrawPoint(pos: Vec3Df, normal: Vec3Df):
    glNormalVec3Df(normal)
    glVertexVec3Df(pos)


def setShaderValues():
    phongShader.setLevels(levels)
    phongShader.setAmbientRef(ambientRef)
    phongShader.setDiffuseRef(diffuseRef)
    phongShader.setSpecularRef(specularRef)
    phongShader.setShininess(shininess)
    phongShader.setRouge(rouge)
    phongShader.setVert(vert)
    phongShader.setBleu(bleu)
   
    
  


def drawMesh(flat: bool):
    V: List[Vertex] = mesh.getVertices()
    T: List[Triangle] = mesh.getTriangles()
    glBegin(GL_TRIANGLES)
    for i in range(len(T)):
        t = T[i]
        if (flat):
            normal = crossProduct(V[t.getVertex(1)].getPos()
                                  - V[t.getVertex(0)].getPos(),
                                  V[t.getVertex(2)].getPos()
                                  - V[t.getVertex(0)].getPos())
            normal.normalize()
            glNormalVec3Df(normal)

        for j in range(3):
            if (not flat):
                glNormalVec3Df(V[t.getVertex(j)].getNormal())
                glVertexVec3Df(V[t.getVertex(j)].getPos())
            else:
                glVertexVec3Df(V[t.getVertex(j)].getPos())
    glEnd()


def drawSolidModel():
    glEnable(GL_LIGHTING)
    glEnable(GL_COLOR_MATERIAL)
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
    glPolygonOffset(1.0, 1.0)
    glEnable(GL_POLYGON_OFFSET_FILL)
    glShadeModel(GL_FLAT)
    phongShader.bind()
    drawMesh(True)
    glPolygonMode(GL_FRONT, GL_LINE)
    glPolygonMode(GL_BACK, GL_FILL)
    glColor3f(0.0, 0.0, 0.0)
    drawMesh(True)
    glDisable(GL_POLYGON_OFFSET_FILL)
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
    glDisable(GL_COLOR_MATERIAL)
    glDisable(GL_LIGHTING)
    glShadeModel(GL_SMOOTH)


def drawPhongModel():
    glCallList(glID)


def initLights():
    light_position_0: List[GLfloat] = [42, 374, 161, 0]
    light_position_1: List[GLfloat] = [473, -351, -259, 0]
    light_position_2: List[GLfloat] = [-438, 167, -48, 0]

    direction_0: List[GLfloat] = [-42, -374, -161]
    direction_1: List[GLfloat] = [-473, 351, 259]
    direction_2: List[GLfloat] = [438, -167, 48]

    diffuse_color_0: List[GLfloat] = [1.0, 1.0, 1.0, 1]
    diffuse_color_1: List[GLfloat] = [0.28, 0.39, 1.0, 1]
    diffuse_color_2: List[GLfloat] = [1.0, 0.69, 0.23, 1]

    specular_color_0: List[GLfloat] = [0.8, 0.0, 0.0, 1]
    specular_color_1: List[GLfloat] = [0.0, 0.8, 0.0, 1]
    specular_color_2: List[GLfloat] = [0.0, 0.0, 0.8, 1]

    ambient: List[GLfloat] = [0.5, 0.5, 0.5, 1.]

    glLightfv(GL_LIGHT0, GL_POSITION, light_position_0)
    glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, direction_0)
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse_color_0)
    glLightfv(GL_LIGHT0, GL_SPECULAR, specular_color_0)

    glLightfv(GL_LIGHT1, GL_POSITION, light_position_1)
    glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, direction_1)
    glLightfv(GL_LIGHT1, GL_DIFFUSE, diffuse_color_1)
    glLightfv(GL_LIGHT1, GL_SPECULAR, specular_color_1)

    glLightfv(GL_LIGHT2, GL_POSITION, light_position_2)
    glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, direction_2)
    glLightfv(GL_LIGHT2, GL_DIFFUSE, diffuse_color_2)
    glLightfv(GL_LIGHT2, GL_SPECULAR, specular_color_2)

    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambient)

    glEnable(GL_LIGHTING)
    glEnable(GL_LIGHT0)
    glEnable(GL_LIGHT1)
    glEnable(GL_LIGHT2)


def setSunriseLight():
    glDisable(GL_LIGHT0)
    glDisable(GL_LIGHT1)
    glDisable(GL_LIGHT2)


def setSingleSpotLight():
    glEnable(GL_LIGHT0)
    glDisable(GL_LIGHT1)
    glDisable(GL_LIGHT2)


def setDefaultMaterial():
    material_color = [1.0, 1.0, 1., 1.0]
    material_specular = [0.5, 0.5, 0.5, 1.0]
    material_ambient = [1.0, 0.0, 0.0, 1.0]

    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, material_specular)
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, material_color)
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, material_ambient)
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 128)


def initGLList():
    global glID
    glID = glGenLists(1)
    glNewList(glID, GL_COMPILE)
    drawMesh(False)
    glEndList()


#
#
def init(filename: str):
    global mesh, phongShader
    glClearColor(0.5, 0.5, 0.5, 1.0)
    glClearDepth(1.0)
    glDepthMask(GL_TRUE)
    glDepthFunc(GL_LESS)
    glEnable(GL_DEPTH_TEST)
    glEnable(GL_CULL_FACE)
    glCullFace(GL_BACK)
    glFrontFace(GL_CCW)
    glShadeModel(GL_SMOOTH)

    initLights()
    setSunriseLight()
    setDefaultMaterial()
    mesh = openOFF(filename, 0)
    initGLList()

    try:
        phongShader = PhongShader("shader.vert", "shader.frag")
        phongShader.bind()
        setShaderValues()
    except Exception as e:
        print("Erreur :", e)
        exit(EXIT_FAILURE)


def clear():
    global phongShader
    phongShader = None
    glDeleteLists(glID, 1)


def reshape(w: int, h: int):
    pass


def display():
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    setShaderValues()
    if mode == Solid:
        drawSolidModel()
    elif mode == Phong or mode == Wire:
        drawPhongModel()
    glFlush()


def idle():
    pass


def printUsage():
    print("""
         --------------------------------------
         TP2
         "--------------------------------------
         USAGE: ./Main <file>.off
         --------------------------------------
         Keyboard commands
         --------------------------------------
          ?: Print help
          w: Toggle wireframe Mode
          f: Toggle full screen mode
          A/a: Increase/Decrease ambient reflection
          D/d: Increase/Decrease diffuse reflection
          S/s: Increase/Decrease specular reflection
          +/-: Increase/Decrease shininess
          <drag>+<left button>: rotate model
          <drag>+<right button>: move model
          <drag>+<middle button>: zoom
          q, <esc>: Quit
         --------------------------------------""")


def key(keyPressed, x: int, y: int):
    global mode, fullScreen, ambientRef, diffuseRef, specularRef, shininess, levels, rouge, vert, bleu
    if keyPressed == 'f':
        if (fullScreen == True):
            glutReshapeWindow(SCREENWIDTH, SCREENHEIGHT)
            fullScreen = False
        else:
            glutFullScreen()
            fullScreen = True
    if keyPressed == 'q':
        clear()
        exit(0)
    if keyPressed == 'w':
        if (mode == Wire):
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
            phongShader.bind()
            mode = Phong
        else:
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)
            phongShader.bind()
            mode = Wire
    if keyPressed == 'A':
        ambientRef += 0.1
    elif keyPressed == 'a':
        ambientRef -= 0.1

    if keyPressed == 'D':
        diffuseRef += 0.1
        print(diffuseRef)
    elif keyPressed == 'd':
        diffuseRef -= 0.1
        print(diffuseRef)

    if keyPressed == 'S':
        specularRef += 0.1
    elif keyPressed == 's':
        specularRef -= 0.1

    if keyPressed == '+':
        shininess += 10.0
       
    elif keyPressed == '-':
        shininess -= 10.0
        
    
    if keyPressed == "N" : 
        levels += 10.0
        
    elif keyPressed == "n" : 
        levels -= 10.0
      
    if keyPressed == "R" : 
        print(rouge)
        rouge += 0.1
        
    elif keyPressed == "r" : 
        print(rouge)
        rouge -= 0.1

    if keyPressed == "G" : 
        print(vert)
        vert += 0.1
        
    elif keyPressed == "g" : 
        print(vert)
        vert -= 0.1

    if keyPressed == "B" : 
        print(bleu)
        bleu += 0.1
        
    elif keyPressed == "b" : 
        print(bleu)
        bleu -= 0.1


    # A compléter : faire varier les variables ambientRef, diffuseRef, specularRef, shininess

    setShaderValues()
    idle()


def mouse(button: int, state: int, x: int, y: int):
    global mouseMovePressed, mouseRotatePressed, mouseZoomPressed
    if (state == GLUT_UP):
        mouseMovePressed = False
        mouseRotatePressed = False
        mouseZoomPressed = False
    else:
        if (button == GLUT_LEFT_BUTTON):
            camera.beginRotate(x, y)
            mouseMovePressed = False
            mouseRotatePressed = True
            mouseZoomPressed = False
        elif (button == GLUT_RIGHT_BUTTON):
            lastX = x
            lastY = y
            mouseMovePressed = True
            mouseRotatePressed = False
            mouseZoomPressed = False
        elif (button == GLUT_MIDDLE_BUTTON):
            if (mouseZoomPressed == False):
                lastZoom = y
                mouseMovePressed = False
                mouseRotatePressed = False
                mouseZoomPressed = True
    idle()


def motion(x: int, y: int):
    pass


def usage():
    printUsage()
    exit(EXIT_FAILURE)


def main():
    global mesh, phongShader
    pg.init()
    windowSize = (500, 500)
    pg.display.set_mode(windowSize, DOUBLEBUF | OPENGL)

    init("data/monkey.off")

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45, (windowSize[0] / windowSize[1]), 0.1, 10.0)
    glMatrixMode(GL_MODELVIEW)
    glTranslatef(0.0, 0.0, -5.0)

    while True:
        for event in pg.event.get():
            if event.type == pg.QUIT:
                pg.quit()
                quit()
            if event.type == pg.KEYDOWN:
                key(event.unicode, 0, 0)

        glRotatef(.5, .5, .5, 1)  # Tourne lentement autour du modèle
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        display()
        pg.display.flip()
        pg.time.wait(10)


if __name__ == "__main__":
    main()
